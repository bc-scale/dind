# Docker in Docker, with extra stuff™

A collection of docker images based on [the official docker image](https://hub.docker.com/_/docker), with some extra packages installed. Consult each variant's Dockerfile in the [variants](./variants) directory to see the additional packages.

Available variants:

- php7

## Usage

Setup [Traefik Proxy](https://gitlab.com/wp-id/docker/traefik-proxy), then use the example compose file in this repository:

```sh
git clone https://gitlab.com/kucrut/dind.git
cd dind/compose
cp .env.example .env
# Edit .env as needed
ln -s docker-compose.override.traefik-proxy.yml docker-compose.override.yml
docker compose up -d
./login
```
